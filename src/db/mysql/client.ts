import * as mysql from 'mysql';

import log from '../../helpers/logger';

import dbConfig from '../../config/db';


const client = mysql.createConnection({
    host: dbConfig.mysql.host,
    port: dbConfig.mysql.port,
    user: dbConfig.mysql.user,
    password: dbConfig.mysql.password,
    database: dbConfig.mysql.database
});


client.connect((err) => {
    if (err) {
        log.error('Mysql connection error' + err);
    } else {
        log.info('Mysql connection success!');
    }
});

export default client;
