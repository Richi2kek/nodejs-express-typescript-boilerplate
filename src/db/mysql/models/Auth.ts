import * as bcrypt from 'bcrypt';

import log from '../../../helpers/logger';

import client from '../client';

import dbConfig from '../../../config/db';

class Auth {
    constructor() {

    }

    getAll(callback: any) {
        client.query('SELECT * FROM users', callback);
    }

    getById(id: number, callback: any) {
        client.query('SELECT * FROM users WHERE id=?',[id], callback);
    }

    add(user: any, callback: any) {
        const userModel = {
            username: user.username,
            email: user.email,
            password: user.password
        };
        this.isUsernameExist(userModel.username, (err: any, result: any) => {
            if (err) { log.error('check username error' + err); }
            console.log('check username result: ', result);
            this.isEmailExist(userModel.email, (err: any, result: any) => {
                if (err) { log.error('check email error' + err); }
                console.log('check email result: ', result);
                client.query('INSERT INTO users SET ?', userModel, callback);
            });
        })
    }

    update(user: any, callback: any) {
        client.query('UPDATE users SET username = ?, email = ?, password = ? WHERE id = ?', [user.username, user.email, user.password, user.id],  callback);
    }

    delete(id: number, callback: any) {
        client.query('DELETE FROM users WHERE id = ?', [id], callback);
    }

    hashPassword(password: string) {
        bcrypt.hash(password, dbConfig.hash.saltRound, (err, hash) => {
            if (err) { log.error('Hash error' + err); }
            return hash;
        });
    }

    checkPassword(password: string, hash: string) {
        bcrypt.compare(password, hash, (err, res) => {
            if (err) {
                log.error(' check password error ' + err);
                return false;
            }
            return true;
        });
    }

    isUsernameExist(username: string, callback: any) {
        client.query('SELECT username FROM users WHERE username = ?', [username], callback);
    }

    isEmailExist(email: string, callback: any) {
        client.query('SELECT email FROM users WHERE email = ?', [email], callback);
    }
}

export default Auth;