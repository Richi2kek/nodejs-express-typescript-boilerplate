export const dbConfig = {
    mysql: {
        host: 'localhost',
        user: 'root',
        port: 3306,
        password: 'root',
        database: 'local_test',
    },
    hash: {
        saltRound: 10
    }
};

export default dbConfig;