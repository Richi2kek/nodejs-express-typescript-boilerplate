
import { Express, Router, Request, Response, NextFunction } from 'express';

const indexRouter: Router = Router();
/* GET home page. */
indexRouter.get('/', (req: Request, res: Response, next: NextFunction) => {
  res.json({
    msg: 'Hello'
  });
});

export default indexRouter;
