import { Express, Router, Request, Response, NextFunction } from 'express';

import log from '../../helpers/logger';

import Auth from '../../db/mysql/models/Auth';

const userRouter: Router = Router();

const auth = new Auth();

userRouter.get('/', (req: Request, res: Response, next: NextFunction) => {
    auth.getAll((err: any, results: any) => {
        if (err) { log.error('Get all users =>' + err);}
        log.get('get all users => ' + results)
        res.json({
            data: results
        });
    });
});

userRouter.get('/:id', (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id;
    auth.getById(id, (err: any, result: any) => {
        if (err) { log.error('Get user by id => ' + err);}
        log.get('Get user by id');
        res.json({
            data: result
        });
    });
});

userRouter.post('/', (req: Request, res: Response, next: NextFunction) => {
    const user = req.body;
    auth.add(user, (err: any) => {
        if (err) { log.error('Post user => ' + err); }
        log.post('Post user');
        res.json({
            data: 'user created'
        });
    });
});

userRouter.put('/', (req: Request, res: Response, next: NextFunction) => {
    const user = req.body;
    auth.update(user, (err: any) => {
        if (err) { log.error('Update user => ' + err ); }
        log.put('Update user');
        res.json({
            data: 'user updated'
        });
    });
})

userRouter.delete('/:id', (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id;
    auth.delete(id, (err: any) => {
        if (err) { log.error('Delete user => ' + err); }
        log.delete('Delete user');
        res.json({
            data: 'user deleted'
        });
    });
});

export default userRouter;
